package com.distribuidora.frontend.logica;

/**
 * Interface requerida para AbstractConverter
 * @author Pablo A. Ramirez paramirez434@gmail.com
 * @version 1.0.0
 * @param <T> Objeto que resibe la clase para convertirlo.
 * @see com.distribuidora.frontend.logica.AbstractConverter
 */
public interface IManagedBean<T>{
    
    /**
     * Metodo que retorna un Objeto apartir de su llave primaria
     * @param i Llave primaria del objeto.
     * @return Un objeto.
     */
    T getObject(Integer i);
}
