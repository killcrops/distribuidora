/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.distribuidora.frontend.security;

import com.distribuidora.backend.entities.Permiso;
import com.distribuidora.backend.entities.Usuario;
import com.distribuidora.frontend.utilities.FacesUtils;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;

/**
 *
 * @author Pablo A. Ramirez paramirez434@gmail.com
 */
@Named(value = "sessionBean")
@SessionScoped
public class SessionBean implements Serializable {

    private Usuario usuario;
    private List<Permiso> permisos;
    public SessionBean() {
    }
    
    @PostConstruct
    public void init(){
        usuario = (Usuario) FacesUtils.getObjectSession("usuario");
        permisos = getUsuario().getIdRol().getPermisoList();
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
    public String cerrarSesion(){
        FacesUtils.removeObjectSession("usuario");
        usuario = null;
        permisos = null;
        return "/index.dis?faces-redirect=true";
    }
    
    public boolean isRol(Integer i){
        return usuario.getIdRol().getIdRol().equals(i);
    }
    
    public boolean deniedRol(Integer i){
        return !usuario.getIdRol().getIdRol().equals(i);
    }
    
    public List<Permiso> getPermisos() {
        return permisos;
    }
    
}
