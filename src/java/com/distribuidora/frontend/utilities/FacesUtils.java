/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.distribuidora.frontend.utilities;

import java.io.IOException;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 * 
 * @author Pablo A. Ramirez paramirez434@gmail.com
 */
public class FacesUtils {
    /**
     * Utilidad que permite generar mensajes.
     * @param titulo  Título que contendra el mensaje.
     * @param contenido Cuerpo del mensaje.
     * @param tipo Es el tipo de mensaje que se creara.<br>
     * <ol>
     * <li>Mensajes danger</li>
     * <li>Mensajes success</li>
     * <li>Mensajes info</li>
     * <li>Mensajes warning</li>
     * </ol>
     */
    public static void showFacesMessage(String titulo, String contenido, int tipo){
        FacesContext context = FacesContext.getCurrentInstance();
        switch(tipo){
            case 1: context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL, titulo, contenido));
                    break;
            case 2: context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR, titulo, contenido));
                    break;
            case 3: context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_INFO, titulo, contenido));
                    break;
            case 4: context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_WARN, titulo, contenido));
                    break;
        }
    }
    
    public static void setObjectSession(String nombre, Object objeto){
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getSessionMap().put(nombre, objeto);
    }
    
    public static Object getObjectSession(String nombre){
        FacesContext context = FacesContext.getCurrentInstance();
        return context.getExternalContext().getSessionMap().get(nombre);
    }
    
    public static void removeObjectSession(String nombre){
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getSessionMap().remove(nombre);
        context.getExternalContext().invalidateSession();
    }
    
    public static void redirect(String dir){
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        try{
            context.getExternalContext().redirect(req.getContextPath() + dir);
        }catch(IOException e){
        }
    }
}