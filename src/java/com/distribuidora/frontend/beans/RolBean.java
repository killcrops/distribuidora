/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.distribuidora.frontend.beans;

import java.io.Serializable;
import javax.ejb.EJB;
import com.distribuidora.backend.entities.Rol;
import com.distribuidora.backend.facades.RolFacade;
import com.distribuidora.frontend.logica.IManagedBean;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author Pablo A. Ramirez paramirez434@gmail.com
 * @version 0.1.0
 */
@Named(value = "rolBean")
@RequestScoped
public class RolBean implements Serializable, IManagedBean<Rol>{
    
    private Rol rol;
    @EJB private RolFacade uf;
    
    public RolBean() {
    }
    
    @PostConstruct
    public void init(){
        rol = new Rol();
    }
    
    @Override
    public Rol getObject(Integer i) {
        return uf.find(i);
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    public List<Rol> listar(){
        return uf.findAll();
    }
    
    public void create(){
        uf.create(rol);
    }
    
    public String iniciarSesion(){
        return "";
    }

}