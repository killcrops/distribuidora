package com.distribuidora.frontend.beans;

import java.io.Serializable;
import javax.ejb.EJB;
import com.distribuidora.backend.entities.Usuario;
import com.distribuidora.backend.facades.UsuarioFacade;
import com.distribuidora.frontend.logica.IManagedBean;
import com.distribuidora.frontend.utilities.FacesUtils;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

/**
 *
 * @author Pablo A. Ramirez paramirez434@gmail.com
 * @version 0.1.0
 */
@Named(value = "usuarioBean")
@RequestScoped
public class UsuarioBean implements Serializable, IManagedBean<Usuario>{
    
    private Usuario usuario;
    @EJB private UsuarioFacade uf;
    @Inject private EstadoUsuarioBean estadoUsuarioBean;
    
    public UsuarioBean() {
    }
    
    @PostConstruct
    public void init(){
        usuario = new Usuario();
    }
    
    @Override
    public Usuario getObject(Integer i) {
        return uf.find(i);
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public EstadoUsuarioBean getEstadoUsuarioBean() {
        return estadoUsuarioBean;
    }

    public List<Usuario> listUsers(){
        return uf.findAll();
    }
    
    public void create(){
        try{
            usuario.setIdEstado(getEstadoUsuarioBean().getObject(1));
            uf.create(usuario);
            FacesUtils.showFacesMessage("Exito", "Usuario: " + getUsuario().getNombres() + " " + getUsuario().getApellidos() + " registrado.", 2);
        }catch(Exception e){
            FacesUtils.showFacesMessage("Exito", "Usuario: " + getUsuario().getUsuario() + " ya exíste.", 1);
        }
    }
    
    public String iniciarSesion(){
        Usuario u;
        try{
            u = uf.atenticar(getUsuario());
            if(u != null){
                FacesUtils.setObjectSession("usuario", u);
                FacesUtils.showFacesMessage("Bienvenido", u.getNombres() + " " + u.getApellidos(), 2);
                return "/seguridad/usuario.dis?faces-redirect=true";
            }
        }catch(Exception e){
        }
        setUsuario(null);
        return "";
    }
    
    public void existeUsuario(){
        if(FacesUtils.getObjectSession("usuario")!=null){
            FacesUtils.redirect("/seguridad/usuario.dis");
        }
    }
}