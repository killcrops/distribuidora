/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.distribuidora.backend.facades;

import com.distribuidora.backend.entities.EstadoUsuario;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * 
 * @author Pablo A. Ramirez paramirez434@gmail.com
 */
@Stateless
public class EstadoUsuarioFacade extends AbstractFacade<EstadoUsuario> {

    @PersistenceContext(unitName = "distribuidoraPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EstadoUsuarioFacade() {
        super(EstadoUsuario.class);
    }

}
